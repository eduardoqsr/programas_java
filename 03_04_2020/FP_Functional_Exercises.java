/*
	Author: @EduardoQSR
	Date: 03, April, 2020
	Subject: Logical and Functional Programming
*/

import java.util.List;
public class FP_Functional_Exercises
{
    public static void main(String[] args)
    {
        List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

        List<String> courses = List.of("Spring", "Spring Boot", "API", "Microsservices",
            "AWS", "PCF", "Azure", "Docker", "Kubernetes");


        System.out.println("\nExercise 1:\n      Print Only Odd Numbers from the List");
        printOddNumbersInListFunctional(numbers);

        System.out.println("\nExercise 2:\n      Print all Courses individually");
        printAllCoursesInListFunctional(courses);

        System.out.println("\nExercise 3:\n      Print Courses Containing the word 'Spring'");
        printCoursesInListFunctionalWithSpring(courses);

        System.out.println("\nExercise 4:\n      Print Courses Whose Name has atleast 4 letters");
        printCoursesInListFunctionalWithFourWords(courses);

        System.out.println("\nExercise 5:\n      Print the cubes of odd numbers");
        printCubeOddNumbersInListFunctional(numbers);

        System.out.println("\nExercise 6:\n      Print the number of characters in each course name");
        printWordsNumberOfCoursesInListFunctional(courses);

    }


    /*Imprimir numeros*/
    private static void printNumber(int number)
    {
        System.out.print(number + ", ");
    }
    /*Imprimir String*/
    private static void printString(String course)
    {
        System.out.print(course + ", ");
    }
    /*Metodo para filtrar la lista*/
    private static boolean isEven(int number)
    {
        return (number % 2 == 0);
    }
    
    
    
    
    
    /* Exercise 1: Print Only Odd Numbers from the List*/
    private static boolean isOdd(int number)
    {
        return (number % 2 == 1);
    }

    private static void printOddNumbersInListFunctional(List<Integer> numbers)
    {
        numbers.stream()                        
            .filter(FP_Functional_Exercises::isOdd)   
            .forEach(FP_Functional_Exercises::printNumber);
        System.out.println("");
    }
    
    
    
    /* Exercise 2: Print all Courses individually*/
    private static void printAllCoursesInListFunctional(List<String> courses)
    {
        courses.stream()                          
            .forEach(FP_Functional_Exercises::printString);
        System.out.println("");
    }


    /*Exercise 3: Print Courses Containing the word "Spring"*/
    private static void printCoursesInListFunctionalWithSpring(List<String> courses)
    {
        courses.stream()
            .filter(course -> course.contains("Spring"))                         
            .forEach(FP_Functional_Exercises::printString);
        System.out.println("");
    }


    /*Exercise 4: Print Courses Whose Name has atleast 4 letters*/
    private static void printCoursesInListFunctionalWithFourWords(List<String> courses)
    {
        courses.stream()
            .filter(course -> course.length() >= 4)                         
            .forEach(FP_Functional_Exercises::printString);
        System.out.println("");
    }


    /*Exercise 5: Print the cubes of odd numbers*/
    private static void printCubeOddNumbersInListFunctional(List<Integer> numbers)
    {
        numbers.stream()                        
            .filter(FP_Functional_Exercises::isOdd)
            .map(number -> number * number)   
            .forEach(FP_Functional_Exercises::printNumber);
        System.out.println("");
    }


    /*Exercise 6: Print the number of characters in each course name*/
    private static void printWordsNumberOfCoursesInListFunctional(List<String> courses)
    {
        courses.stream()
            .map(course -> course.length())                         
            .forEach(FP_Functional_Exercises::printNumber);
        System.out.println("");
    }
}
