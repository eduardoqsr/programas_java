#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>

int validar_Numeros(char numero[]);
void strrev(char* str);
int chrtoint(char a);
void calcular_Suma();

int main()
{
	int opc;
	do
	{
		printf("\n\nIngresa opcion:\n");
		printf("    1.- Sumar dos numeros muy grandes.\n");
		printf("    2.- Salir.\n");
		scanf("%d", &opc);
		switch(opc)
		{
			case 1:
				calcular_Suma();
				break;
			case 2:
				printf("Opcion SALIR");
				break;
			default:
				printf("Error... Opcion incorrecta");
				break;
		}
	}while(opc != 2);
	
	return 0;
}

int chrtoint(char a)
{
	int i;
	for (i = 48; i <= 57; i++)
	{
		if (toascii(i)==a)
		{
			return i-48;
		}
	}
	return 0;
}

int validar_Numeros(char numero[])
{
	int i = 0, sw = 0, j;
	j = strlen(numero);
	while(i < j && sw == 0)
	{
		if(isdigit(numero[i]) != 0)
		{
			i ++;
		}else
		{
			sw = 1;
		}
	}
	return sw;
}

void calcular_Suma()
{
	char numero_Uno[250];
	char numero_Dos[250];
	int resultado[250];
	int c1, c2;
	int i, j, m, cmax, suma;
	printf("Ingresa el primer numero :");
	scanf("%s", &numero_Uno);
	printf("\nIngresa el segundo numero :");
	scanf("%s", &numero_Dos);
	
	int aux1 = validar_Numeros(numero_Uno);
	int aux2 = validar_Numeros(numero_Dos);
	
	if(aux1 == 0 && aux2 == 0)
	{
		c1 = strlen(numero_Uno);
		c2 = strlen(numero_Dos);
		
		strrev(numero_Uno);
		strrev(numero_Dos);
		cmax = c1;
		
		if(c1 < c2)
		{
			cmax = c2;
		}
		m = 0;
		for(i = 0; i < cmax; i ++)
		{
			if(c1==c2 || (i < c1 && i < c2))
			{
				suma = m+chrtoint(numero_Uno[i]) + chrtoint(numero_Dos[i]);
			}else if(i >= c1)
			{
				suma = m + chrtoint(numero_Dos[i]);
			}else if(i >= c2)
			{
				suma = m + chrtoint(numero_Uno[i]);
			}
			resultado[i] = suma % 10;
			m = suma / 10;
		}
		if(m)
		{
			resultado[i] = m;
			i ++;
		}
		printf("\nResultado: ");
		for(j = 0; j < i; j++)
		{
			printf("%d", resultado[i-j-1]);
		}
		printf("\n");
	}else
	{
		printf("\n--------------------------------------------");
		printf("\n|ERROR... Ingresa numeros enteros positivos|");
		printf("\n--------------------------------------------\n");
		getchar();
	}
}

void strrev(char* str)
{
	char temporal;
	for(int i = 0, j = strlen(str)-1; i < j; j--, i++)
	{
		temporal = str[i];
		str[i] = str[j];
		str[j] = temporal;
	}
}
